@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ingreso de Nuevos Clientes</div>

                <div class= "col text-right">
                    <a href="{{ route('list.cliente') }}" class="btn btn-sm btn-success">Cancelar</a>
                </div>

                <div class="card-body">

                    <form role="form" method="POST" action= "{{ route('guardar.cliente')}}" >
                        {{ csrf_field() }}
                        {{ method_field('post') }}
                        <div class="row">
                            <div class="col-lg-4">
                            <label class="from-control-label" for="Nombre">Nombre</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>
 
                        <div class="col-lg-4">
                            <label class="from-control-label" for="Apellidos">Apellidos</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>

            
                        <div class="col-lg-4">
                            <label class="from-control-label" for="Cédula">Cédula</label>
                            <input type="number" class="from-control" name="nombre">
                        </div>

                        <div class="col-lg-4">
                            <label class="from-control-label" for="Dirección">Dirección</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>
                        
                        <div class="col-lg-4">
                            <label class="from-control-label" for="Teléfono">Teléfono</label>
                            <input type="number" class="from-control" name="nombre">
                        </div>

                        <div class="col-lg-4">
                            <label class="from-control-label" for="Fecha_nacimiento">Fecha de Nacimiento</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>

                        <div class="col-lg-4">
                            <label class="from-control-lable" for="Email">Email</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>

                        </div>

                        <button type="submit" class="btn btn-success pull-right"> Guardar </button>

                    </form>





                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection