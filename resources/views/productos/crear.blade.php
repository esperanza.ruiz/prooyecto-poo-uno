@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('CREAR PRODUCTO') }}</div>

                <div class= "col text-right">
                    <a href="{{ route('list.productos') }}" class="btn btn-sm btn-success">Cancelar</a>
                </div>

                <div class="card-body">

                    <form role="form" method="POST" action="{{ route('guardar.productos')}}">
                        {{ csrf_field() }}
                        {{ method_field('post') }}
                        <div class="row">
                            <div class="col-lg-4">
                            <label class="from-control-label" for="nombre">Nombre del Producto</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>

                        
                        <div class="col-lg-4">
                            <label class="from-control-label" for="tipo">Tipo de Producto</label>
                            <input type="text" class="from-control" name="tipo">
                        </div>

                        
                        <div class="col-lg-4">
                            <label class="from-control-label" for="estado">Estado del Producto</label>
                            <input type="number" class="from-control" name="estado">
                        </div>

                        
                        <div class="col-lg-4">
                            <label class="from-control-label" for="precio">Precio del Producto</label>
                            <input type="number" class="from-control" name="precio">
                        </div>
                        </div>

                        <button type="submit" class="btn btn-success pull-right"> Guardar </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection