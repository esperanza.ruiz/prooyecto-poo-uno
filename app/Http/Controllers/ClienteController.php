<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InicioClientes(Request $request)
    {
        //dd('Hola Mundo2');
        $cliente = Cliente::all();
        //dd($cliente);
        return view('Clientes.inicio')->with('cliente', $cliente);
    }

    public function CrearClientes(Request $request) 
    {
        $cliente = Cliente::all();
        return view('Clientes.crear')->with('cliente', $cliente);
    }

    public function GuardarCliente(Request $request)
     {
        $this->validate($request,[
            
            'Nombre'=> 'required',
            'Apellidos'=> 'required',
            'Cédula'=> 'required',
            'Dirección'=> 'required',
            'Teléfono'=> 'required',
            'Fecha'=> 'required',
            'Email'=> 'required'
          
        ]);

        $producto = new liente;
        $producto->Nombre          = $request->Nombre;
        $producto->Apellidos       = $request->Apellidos;
        $producto->Cédula          = $request->Cédula;
        $producto->Dirección       = $request->Dirección;
        $producto->Teléfono        = $request->Teléfono;
        $producto->Fecha_nacimiento= $request->Fecha_nacimiento;
        $producto->Email           = $request->Email;
        $producto->save();
        
        return redirect()->route('list.Clientes');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
