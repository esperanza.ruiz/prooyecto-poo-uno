<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'ide',
        'nombre',
        'apellidos',
        'cedula',
        'dirección',
        'teléfono',
        'fecha_nacimiento',
        'email'
    ];
}
